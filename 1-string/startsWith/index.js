export default function collectCarNumberCount(collection) {
  return collection.filter(item => item.match('^粤A\\w+')).length;
}

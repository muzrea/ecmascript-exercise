export default function find00OldPerson(collection) {
  return collection.filter(item => item.age < 19 && item.age > 9).map(item => item.name)[0];
}

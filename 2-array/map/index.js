export default function numberMapToWord(collection) {
  return collection.map(item => String.fromCharCode(96 + item));
}
